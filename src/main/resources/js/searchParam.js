async function searchParam() {
    try {
        const response = await fetch(
            'http://localhost:8080/api/persons/search?${query}',
            {
                method: 'GET',
                query: params
            },
        );

        if (!response.ok) {
            throw new Error(`Error! status: ${response.status}`);
        }

        return await response.json();
    } catch (error) {
        console.log(error);
    }
}

searchParam().then(data => {
    console.log(data);

    const preElement = document.getElementById('json-data');

    preElement.style.fontSize = '18px';

    preElement.innerHTML = JSON.stringify(data, null, 2);
});
