function submitForm() {
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var description = document.getElementById("description").value;

    try {
        // Send data to the backend
        fetch('http://localhost:8080/api/persons', {
            method: 'POST',
            mode: "cors",
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName, lastName: lastName,
                email: email, phone: phone, description: description
            })
        })
            .then(response => response.json())
            .then(data => {
                // Handle the response from the backend
                console.log(data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    } catch (error) {
        console.error('Error:', error);
    }
    console.log('попали');
}