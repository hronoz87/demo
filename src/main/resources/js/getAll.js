async function getAll() {
    try {
        const response = await fetch(
            'http://localhost:8080/api/persons/all',
            {
                method: 'GET',
            },
        );

        if (!response.ok) {
            throw new Error(`Error! status: ${response.status}`);
        }

        return await response.json();
    } catch (error) {
        console.log(error);
    }
}

getAll().then(data => {
    console.log(data);

    const preElement = document.getElementById('json-data');

    preElement.style.fontSize = '18px';

    preElement.innerHTML = JSON.stringify(data, null, 2);
});
