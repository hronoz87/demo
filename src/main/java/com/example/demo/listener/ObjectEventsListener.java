package com.example.demo.listener;

import com.example.demo.events.AddEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class ObjectEventsListener {
    @Autowired
    private DataSource dataSource;

    @EventListener
    public void handleAddPersonEvent(AddEvent event) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("obj", ow.writeValueAsString(event.getObj()));
            parameters.put("action_event", event.getActionEvent());
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                    .withTableName("events")
                    .usingGeneratedKeyColumns("id", "created");
            simpleJdbcInsert.execute(parameters);
            log.info("Person {}: {}", event.getActionEvent(), event.getObj());
        } catch (JsonProcessingException e) {
            // catch various errors
            e.printStackTrace();
        }
    }
}
