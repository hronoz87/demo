package com.example.demo.dao;

import com.example.demo.dto.Customer;
import com.example.demo.dto.CustomerPaymentMethod;
import com.example.demo.dto.Order;
import com.example.demo.dto.OrderProduct;
import com.querydsl.core.dml.StoreClause;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.QBean;
import com.querydsl.sql.SQLQueryFactory;
import com.querydsl.sql.dml.SQLInsertClause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.demo.qentity.QCustomer.customer;
import static com.example.demo.qentity.QCustomerOrder.customerOrder;
import static com.example.demo.qentity.QCustomerOrderProduct.customerOrderProduct;
import static com.example.demo.qentity.QCustomerPaymentMethod.customerPaymentMethod;
import static com.querydsl.core.types.Projections.bean;

@Transactional
public class OrderDaoImpl implements OrderDao {

    @Autowired
    SQLQueryFactory queryFactory;

    final QBean<OrderProduct> orderProductBean = bean(OrderProduct.class,
            customerOrderProduct.productId, customerOrderProduct.comments, customerOrderProduct.quantity);

    final QBean<Order> orderBean = bean(Order.class,
            customerOrder.id, customerOrder.orderPlacedDate, customerOrder.orderPaidDate,
            customerOrder.orderStatus, customerOrder.totalOrderPrice,
            bean(CustomerPaymentMethod.class, customerPaymentMethod.all()).as("customerPaymentMethod"),
            bean(Customer.class, customer.all()).as("customer"),
            GroupBy.set(orderProductBean).as("orderProducts"));

    @Override
    public Order findById(long id) {
        List<Order> orders = findAll(customerOrder.id.eq(id));
        return orders.isEmpty() ? null : orders.get(0);
    }

    @Override
    public List<Order> findAll(Predicate... where) {
        return queryFactory.from(customerOrder)
                .leftJoin(customerOrder.customerIdForeign, customerPaymentMethod)
                .leftJoin(customerOrder._orderFk, customerOrderProduct)
                .leftJoin(customerOrder._customerFk, customer)
                .where(where)
                .transform(GroupBy.groupBy(customerOrder.id).list(orderBean));
    }

    private <T extends StoreClause<T>> T populate(T dml, Order o) {
        return dml.set(customerOrder.customerPaymentMethodId, o.getCustomerPaymentMethod().getId())
                .set(customerOrder.customerId, o.getCustomerPaymentMethod().getCustomerId())
                .set(customerOrder.orderStatus, o.getOrderStatus())
                .set(customerOrder.orderPlacedDate, o.getOrderPlacedDate())
                .set(customerOrder.totalOrderPrice, o.getTotalOrderPrice());
    }

    @Override
    public Order save(Order o) {
        Long id = o.getId();

        if (id == null) {
            id = populate(queryFactory.insert(customerOrder), o)
                    .executeWithKey(customerOrder.id);
            o.setId(id);
        } else {
            populate(queryFactory.update(customerOrder), o)
                    .where(customerOrder.id.eq(id))
                    .execute();

            // delete orderproduct rows
            queryFactory.delete(customerOrderProduct)
                    .where(customerOrderProduct.orderId.eq(id))
                    .execute();
        }
        SQLInsertClause insertPayment = queryFactory.insert(customerPaymentMethod);
        insertPayment.set(customerPaymentMethod.id, id)
                .set(customerPaymentMethod.customerId, o.getCustomerPaymentMethod().getCustomerId())
                .set(customerPaymentMethod.cardNumber, o.getCustomerPaymentMethod().getCardNumber())
                .set(customerPaymentMethod.otherDetails, o.getCustomerPaymentMethod().getOtherDetails())
                .set(customerPaymentMethod.paymentMethodCode, o.getCustomerPaymentMethod().getPaymentMethodCode())
                .addBatch();
        insertPayment.execute();

        SQLInsertClause insert = queryFactory.insert(customerOrderProduct);
        for (OrderProduct op : o.getOrderProducts()) {
            insert.set(customerOrderProduct.orderId, id)
                    .set(customerOrderProduct.comments, op.getComments())
                    .set(customerOrderProduct.productId, op.getProductId())
                    .set(customerOrderProduct.quantity, op.getQuantity())
                    .addBatch();
        }
        insert.execute();

        o.setId(id);
        return o;
    }

    @Override
    public long count() {
        return queryFactory.from(customerOrder).fetchCount();
    }

    @Override
    public void delete(long id) {
        // TODO use combined delete clause
        queryFactory.delete(customerOrderProduct)
                .where(customerOrderProduct.orderId.eq(id))
                .execute();

        queryFactory.delete(customerOrder)
                .where(customerOrder.id.eq(id))
                .execute();
    }

}
