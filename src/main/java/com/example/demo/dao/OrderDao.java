package com.example.demo.dao;

import com.example.demo.dto.Order;
import com.querydsl.core.types.Predicate;

import java.util.List;

public interface OrderDao {

    Order findById(long id);

    List<Order> findAll(Predicate... where);

    Order save(Order order);

    long count();

    void delete(long id);

}
