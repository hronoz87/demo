package com.example.demo.dao;

import com.example.demo.dto.Customer;
import com.querydsl.core.types.Predicate;

import java.util.List;

public interface CustomerDao {

    Customer findById(long id);

    List<Customer> findAll(Predicate... where);

    Customer save(Customer c);

    long count();

    void delete(long id);

}
