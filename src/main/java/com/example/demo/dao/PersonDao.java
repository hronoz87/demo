package com.example.demo.dao;

import com.example.demo.dto.Person;
import com.querydsl.core.types.Predicate;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface PersonDao {

    Person findById(long id);

    List<Person> findAll(Predicate... where);
    List<Person> findByOneParam(String param);

    Person save(Person p);

    long count();

    void delete(long id);

    List<Person> findByParams(Map<String, String> paramsSearch);

    List<Person> findPg(String param);

}
