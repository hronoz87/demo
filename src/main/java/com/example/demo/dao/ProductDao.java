package com.example.demo.dao;

import com.example.demo.dto.Product;
import com.querydsl.core.types.Predicate;

import java.util.List;

public interface ProductDao {

    Product findById(long id);

    List<Product> findAll(Predicate... where);

    Product save(Product p);

    long count();

    void delete(long id);

}
