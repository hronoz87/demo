package com.example.demo.dao;

import com.example.demo.dto.ActionEvent;
import com.example.demo.dto.Person;
import com.example.demo.events.AddEvent;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.QBean;
import com.querydsl.sql.SQLQueryFactory;
import liquibase.database.DatabaseConnection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.example.demo.qentity.QPerson.person;
import static com.querydsl.core.types.Projections.bean;

@Transactional
@Slf4j
@Component
public class PersonDaoImpl implements PersonDao {
    @Autowired
    public NamedParameterJdbcTemplate template;

    @Autowired
    SQLQueryFactory queryFactory;
    @Autowired
    ApplicationEventPublisher applicationEventPublisher;
    final QBean<Person> personBean = bean(Person.class, person.all());


    @Override
    public Person findById(long id) {
        return queryFactory.select(personBean)
                .from(person)
                .where(person.id.eq(id))
                .fetchOne();
    }

    @Override
    public List<Person> findByOneParam(String param) {
        return queryFactory.select(personBean)
                .from(person)
                .where(
                        person.firstName.eq(param)
                                .or(person.lastName.eq(param))
                                .or(person.phone.eq(param))
                                .or(person.email.eq(param))
                )
                .fetch();
    }

    @Override
    public List<Person> findAll(Predicate... where) {
        return queryFactory.select(personBean)
                .from(person)
                .where(where)
                .fetch();
    }

    @Override
    public Person save(Person p) {
        Long id = p.getId();

        if (id == null) {
            id = queryFactory.insert(person)
                    .populate(p)
                    .executeWithKey(person.id);
            p.setId(id);
            applicationEventPublisher.publishEvent(new AddEvent(p, ActionEvent.CREATED));
        } else {
            queryFactory.update(person)
                    .populate(p)
                    .where(person.id.eq(id)).execute();
        }

        return p;
    }

    @Override
    public long count() {
        return queryFactory.from(person).fetchCount();
    }

    @Override
    public void delete(long id) {
        if (findById(id) != null) {
            applicationEventPublisher.publishEvent(new AddEvent(findById(id), ActionEvent.DELETED));
        }
        queryFactory.delete(person)
                .where(person.id.eq(id))
                .execute();
    }

    @Override
    public List<Person> findByParams(Map<String, String> paramsSearch) {
        Predicate build = build(paramsSearch);
        return queryFactory.select(personBean)
                .from(person)
                .where(build)
                .fetch();
    }

    public Predicate build(Map<String, String> paramsSearch) {
        return new OptionalBooleanBuilder(person.isNotNull())
                .notEmptyAnd(person.firstName::containsIgnoreCase, paramsSearch.get(person.firstName.getMetadata().getName()))
                .notEmptyAnd(person.lastName::containsIgnoreCase, paramsSearch.get(person.lastName.getMetadata().getName()))
                .notEmptyAnd(person.email::containsIgnoreCase, paramsSearch.get(person.email.getMetadata().getName()))
                .notEmptyAnd(person.phone::containsIgnoreCase, paramsSearch.get(person.phone.getMetadata().getName()))
                .build();
    }

    public List<Person> findPg(String text) {
        return template.query("select * from person WHERE word_similarity(person.first_name, :text) > 0.4",
                new MapSqlParameterSource("text", text),
                new BeanPropertyRowMapper<>(Person.class));
    }
}
