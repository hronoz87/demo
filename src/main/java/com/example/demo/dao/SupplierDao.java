package com.example.demo.dao;

import com.example.demo.dto.Supplier;
import com.querydsl.core.types.Predicate;

import java.util.List;

public interface SupplierDao {

    Supplier findById(long id);

    List<Supplier> findAll(Predicate... where);

    Supplier save(Supplier s);

    long count();

    void delete(long s);

}
