package com.example.demo.dto;

import lombok.Data;

@Data
public class ProductL10n {

    private String description;

    private String lang;

    private String name;

}

