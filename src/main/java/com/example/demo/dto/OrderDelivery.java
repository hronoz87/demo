package com.example.demo.dto;

import lombok.Data;
import org.joda.time.LocalDateTime;

@Data
public class OrderDelivery {

    private String deliveryStatusCode;

    private LocalDateTime reportedDate;

}

