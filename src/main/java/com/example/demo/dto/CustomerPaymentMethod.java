package com.example.demo.dto;

import lombok.Data;
import org.joda.time.LocalDateTime;

@Data
public class CustomerPaymentMethod {

    private Long id;

    private Long customerId;

    private String cardNumber, otherDetails, paymentMethodCode;

    private LocalDateTime fromDate, toDate;

}

