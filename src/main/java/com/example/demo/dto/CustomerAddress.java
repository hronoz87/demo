package com.example.demo.dto;

import lombok.Data;
import org.joda.time.LocalDateTime;

@Data
public class CustomerAddress {

    private Address address;

    private String addressTypeCode;

    private LocalDateTime fromDate, toDate;

}

