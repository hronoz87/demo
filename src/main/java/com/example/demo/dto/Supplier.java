package com.example.demo.dto;

import lombok.Data;

@Data
public class Supplier {

    private Long id;

    private String code;

    private String name;

}

