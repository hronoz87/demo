package com.example.demo.dto;

import lombok.Data;
import org.joda.time.LocalDateTime;

import java.util.Set;

@Data
public class Order {

    private Long id;

    private CustomerPaymentMethod customerPaymentMethod;

    private LocalDateTime orderPlacedDate, orderPaidDate;

    private String orderStatus;

    private Double totalOrderPrice;

    private Set<OrderProduct> orderProducts;

}

