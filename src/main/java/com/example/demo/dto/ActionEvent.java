package com.example.demo.dto;

public enum ActionEvent {
    CREATED,
    DELETED,
    UPDATE,
    SEARCH
}
