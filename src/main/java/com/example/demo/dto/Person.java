package com.example.demo.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
public class Person {

    private Long id;

    private String firstName;
    private String lastName;
    @Email
    private String email;
    @Pattern(regexp = "\\+\\d(-\\d{3}){2}-\\d{4}")
    private String phone;
    private String description;
}

