package com.example.demo.controller;

import com.example.demo.dao.PersonDao;
import com.example.demo.dao.SupplierDao;
import com.example.demo.dto.Person;
import com.example.demo.dto.Supplier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/suppliers")
public class SupplierController {
    private final SupplierDao supplierDao;

    public SupplierController(SupplierDao supplierDao) {
        this.supplierDao = supplierDao;
    }

    @PostMapping
    public ResponseEntity<Supplier> save(@RequestBody Supplier supplier) {
        Supplier createdSupplier = supplierDao.save(supplier);
        return ResponseEntity.ok(createdSupplier);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        supplierDao.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Supplier> findById(@PathVariable Long id) {
        Supplier supplier = supplierDao.findById(id);
        return ResponseEntity.ok(supplier);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Supplier>> findAll() {
        List<Supplier> suppliers = supplierDao.findAll();
        return ResponseEntity.ok(suppliers);
    }

//    @GetMapping
//    public ResponseEntity<List<Person>> findByFirstNameAndLastName(@RequestParam String firstName, @RequestParam String lastName) {
//        List<Person> persons = personService.findByFirstNameAndLastName(firstName, lastName);
//        return ResponseEntity.ok(persons);
//    }

//    @GetMapping("/search")
//    public ResponseEntity<List<Person>> findByFullTextSearch(@RequestParam String searchTerm) {
//        List<Person> persons = personService.findByFullTextSearch(searchTerm);
//        return ResponseEntity.ok(persons);
//    }
}
