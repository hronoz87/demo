package com.example.demo.controller;

import com.example.demo.dao.CustomerDao;
import com.example.demo.dao.PersonDao;
import com.example.demo.dto.Customer;
import com.example.demo.dto.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    private final CustomerDao customerDao;

    public CustomerController(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @PostMapping
    public ResponseEntity<Customer> save(@RequestBody Customer customer) {
        Customer createdCustomer = customerDao.save(customer);
        return ResponseEntity.ok(createdCustomer);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        customerDao.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> findById(@PathVariable Long id) {
        Customer customer = customerDao.findById(id);
        return ResponseEntity.ok(customer);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Customer>> findAll() {
        List<Customer> customers = customerDao.findAll();
        return ResponseEntity.ok(customers);
    }

//    @GetMapping
//    public ResponseEntity<List<Person>> findByFirstNameAndLastName(@RequestParam String firstName, @RequestParam String lastName) {
//        List<Person> persons = personService.findByFirstNameAndLastName(firstName, lastName);
//        return ResponseEntity.ok(persons);
//    }

//    @GetMapping("/search")
//    public ResponseEntity<List<Person>> findByFullTextSearch(@RequestParam String searchTerm) {
//        List<Person> persons = personService.findByFullTextSearch(searchTerm);
//        return ResponseEntity.ok(persons);
//    }
}
