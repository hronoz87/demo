package com.example.demo.controller;

import com.example.demo.dao.OrderDao;
import com.example.demo.dao.PersonDao;
import com.example.demo.dto.Order;
import com.example.demo.dto.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    private final OrderDao orderDao;

    public OrderController(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @PostMapping
    public ResponseEntity<Order> save(@RequestBody Order order) {
        Order createdOrder = orderDao.save(order);
        return ResponseEntity.ok(createdOrder);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        orderDao.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Order> findById(@PathVariable Long id) {
        Order order = orderDao.findById(id);
        return ResponseEntity.ok(order);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Order>> findAll() {
        List<Order> orders = orderDao.findAll();
        return ResponseEntity.ok(orders);
    }

//    @GetMapping
//    public ResponseEntity<List<Person>> findByFirstNameAndLastName(@RequestParam String firstName, @RequestParam String lastName) {
//        List<Person> persons = personService.findByFirstNameAndLastName(firstName, lastName);
//        return ResponseEntity.ok(persons);
//    }

//    @GetMapping("/search")
//    public ResponseEntity<List<Person>> findByFullTextSearch(@RequestParam String searchTerm) {
//        List<Person> persons = personService.findByFullTextSearch(searchTerm);
//        return ResponseEntity.ok(persons);
//    }
}
