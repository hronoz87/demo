package com.example.demo.controller;

import com.example.demo.dao.PersonDao;
import com.example.demo.dao.ProductDao;
import com.example.demo.dto.Person;
import com.example.demo.dto.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    private final ProductDao productDao;

    public ProductController(ProductDao productDao) {
        this.productDao = productDao;
    }

    @PostMapping
    public ResponseEntity<Product> save(@RequestBody Product product) {
        Product createdProduct = productDao.save(product);
        return ResponseEntity.ok(createdProduct);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        productDao.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable Long id) {
        Product product = productDao.findById(id);
        return ResponseEntity.ok(product);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Product>> findAll() {
        List<Product> products = productDao.findAll();
        return ResponseEntity.ok(products);
    }

//    @GetMapping
//    public ResponseEntity<List<Person>> findByFirstNameAndLastName(@RequestParam String firstName, @RequestParam String lastName) {
//        List<Person> persons = personService.findByFirstNameAndLastName(firstName, lastName);
//        return ResponseEntity.ok(persons);
//    }

//    @GetMapping("/search")
//    public ResponseEntity<List<Person>> findByFullTextSearch(@RequestParam String searchTerm) {
//        List<Person> persons = personService.findByFullTextSearch(searchTerm);
//        return ResponseEntity.ok(persons);
//    }
}
