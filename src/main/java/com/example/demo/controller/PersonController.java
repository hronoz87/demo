package com.example.demo.controller;

import com.example.demo.dao.PersonDao;
import com.example.demo.dto.Person;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping("/api/persons")
public class PersonController {
    @Autowired
    private PersonDao personDao;

//    public PersonController(PersonDao personDao) {
//        this.personDao = personDao;
//    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Person> save(@Valid @RequestBody Person person) {
        return new ResponseEntity<>(personDao.save(person), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        personDao.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Person> findById(@PathVariable Long id) {
        Person person = personDao.findById(id);
        return ResponseEntity.ok(person);
    }

    @GetMapping("/all")
    public ResponseEntity<List<Person>> findAll() {
        List<Person> persons = personDao.findAll();
        return ResponseEntity.ok(persons);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Person>> findByParams(@RequestParam Map<String, String> paramsSearch) {
        List<Person> persons = personDao.findByParams(paramsSearch);
        return ResponseEntity.ok(persons);
    }

    @GetMapping("/search/{param}")
    public ResponseEntity<List<Person>> findByParam(@PathVariable String param) {
        List<Person> persons = personDao.findByOneParam(param);
        return ResponseEntity.ok(persons);
    }

    @GetMapping("/text/{param}")
    public ResponseEntity<List<Person>> findPg(@PathVariable String param) {
        List<Person> persons = personDao.findPg(param);
        return ResponseEntity.ok(persons);
    }
}
