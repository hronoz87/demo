package com.example.demo.events;

import com.example.demo.dto.ActionEvent;
import lombok.Data;

@Data
public class AddEvent {

    Object obj;
    ActionEvent actionEvent;

    public AddEvent(Object obj, ActionEvent actionEvent) {
        this.obj = obj;
        this.actionEvent = actionEvent;
    }
}
