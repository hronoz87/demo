package com.example.demo.qentity;

import com.example.demo.dto.Person;
import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.RelationalPathBase;

import static com.example.demo.qentity.QPerson.person;

public class QCustomer extends RelationalPathBase<QCustomer> {

    public static final QCustomer customer = new QCustomer("CUSTOMER");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> contactPersonId = createNumber("contactPersonId", Long.class);

    public final StringPath name = createString("name");
    public ForeignKey<QCustomerAddress> _customer3Fk = createForeignKey(id, "customer_id");
    public ForeignKey<QPerson> contactPersonFk = createForeignKey(contactPersonId, "id");

    public QCustomer(Class<? extends QCustomer> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QCustomer(Class<? extends QCustomer> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QCustomer(String variable) {
        super(QCustomer.class, PathMetadataFactory.forVariable(variable), "", "CUSTOMER");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(contactPersonId, ColumnMetadata.named("contact_person_id"));
        addMetadata(name, ColumnMetadata.named("name"));
    }
}
