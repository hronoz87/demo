package com.example.demo.qentity;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.RelationalPathBase;

public class QProductL10n extends RelationalPathBase<QProductL10n> {

    public static final QProductL10n productL10n = new QProductL10n("PRODUCT_L10N");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> productId = createNumber("productId", Long.class);
    public final StringPath name = createString("name");
    public final StringPath lang = createString("lang");
    public final StringPath description = createString("description");

    public QProductL10n(Class<? extends QProductL10n> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QProductL10n(Class<? extends QProductL10n> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QProductL10n(String variable) {
        super(QProductL10n.class, PathMetadataFactory.forVariable(variable), "", "PRODUCT_L10N");
        addMetadata();
    }
    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(productId, ColumnMetadata.named("product_id"));
        addMetadata(name, ColumnMetadata.named("name"));
        addMetadata(lang, ColumnMetadata.named("lang"));
        addMetadata(description, ColumnMetadata.named("description"));
    }
}
