package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;

public class QSupplier extends RelationalPathBase<QSupplier> {

    public static final QSupplier supplier = new QSupplier("SUPPLIER");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath code = createString("code");
    public final StringPath name = createString("name");

    public QSupplier(Class<? extends QSupplier> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QSupplier(Class<? extends QSupplier> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QSupplier(String variable) {
        super(QSupplier.class, PathMetadataFactory.forVariable(variable), "", "SUPPLIER");
        addMetadata();
    }
    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(code, ColumnMetadata.named("code"));
        addMetadata(name, ColumnMetadata.named("name"));
    }
}
