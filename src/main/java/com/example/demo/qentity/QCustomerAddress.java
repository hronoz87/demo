package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.RelationalPathBase;
import org.joda.time.LocalDateTime;

import static com.example.demo.qentity.QAddress.address;

public class QCustomerAddress extends RelationalPathBase<QCustomerAddress> {

    public static final QCustomerAddress customerAddress = new QCustomerAddress("CUSTOMER_ADDRESS");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> customerId = createNumber("customerId", Long.class);
    public final NumberPath<Long> addressId = createNumber("addressId", Long.class);
    public final DateTimePath<LocalDateTime> fromDate = createDateTime("fromDate", LocalDateTime.class);
    public final DateTimePath<LocalDateTime> toDate = createDateTime("toDate", LocalDateTime.class);
    public final StringPath addressTypeCode = createString("addressTypeCode");

    public ForeignKey<QAddress> addressFk = createForeignKey(addressId, "id");

    public QCustomerAddress(Class<? extends QCustomerAddress> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QCustomerAddress(Class<? extends QCustomerAddress> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QCustomerAddress(String variable) {
        super(QCustomerAddress.class, PathMetadataFactory.forVariable(variable), "", "CUSTOMER_ADDRESS");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(customerId, ColumnMetadata.named("customer_id"));
        addMetadata(addressId, ColumnMetadata.named("address_id"));
        addMetadata(fromDate, ColumnMetadata.named("from_date"));
        addMetadata(toDate, ColumnMetadata.named("to_date"));
        addMetadata(addressTypeCode, ColumnMetadata.named("address_type_code"));
    }
}
