package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.PrimaryKey;
import com.querydsl.sql.RelationalPathBase;

public class QPerson extends RelationalPathBase<QPerson> {
    public static final QPerson person = new QPerson("PERSON");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath firstName = createString("firstName");
    public final StringPath lastName = createString("lastName");
    public final StringPath phone = createString("phone");
    public final StringPath email = createString("email");
    public final StringPath description = createString("description");


    public QPerson(Class<? extends QPerson> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QPerson(Class<? extends QPerson> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QPerson(String variable) {
        super(QPerson.class, PathMetadataFactory.forVariable(variable), "", "PERSON");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(firstName, ColumnMetadata.named("first_name"));
        addMetadata(lastName, ColumnMetadata.named("last_name"));
        addMetadata(phone, ColumnMetadata.named("phone"));
        addMetadata(email, ColumnMetadata.named("email"));
        addMetadata(description, ColumnMetadata.named("description"));
    }
}
