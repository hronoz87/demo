package com.example.demo.qentity;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.RelationalPathBase;
import org.joda.time.LocalDateTime;

import static com.example.demo.qentity.QCustomerPaymentMethod.customerPaymentMethod;


public class QCustomerOrder extends RelationalPathBase<QCustomerOrder> {

    public static final QCustomerOrder customerOrder = new QCustomerOrder("CUSTOMER_ORDER");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> customerId = createNumber("customerId", Long.class);
    public final NumberPath<Long> customerPaymentMethodId = createNumber("customerPaymentMethodId", Long.class);
    public final NumberPath<Double> totalOrderPrice = createNumber("totalOrderPrice", Double.class);
    public final DateTimePath<LocalDateTime> orderPlacedDate = createDateTime("orderPlacedDate", LocalDateTime.class);
    public final DateTimePath<LocalDateTime> orderPaidDate = createDateTime("orderPaidDate", LocalDateTime.class);
    public final StringPath orderStatus = createString("orderStatus");

    public ForeignKey<QCustomerPaymentMethod> customerIdForeign = createForeignKey(customerPaymentMethodId, "id");
    public ForeignKey<QCustomerOrderProduct> _orderFk = createForeignKey(id, "order_id");
    public ForeignKey<QCustomer> _customerFk  = createForeignKey(customerId, "id");

    public QCustomerOrder(Class<? extends QCustomerOrder> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QCustomerOrder(Class<? extends QCustomerOrder> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QCustomerOrder(String variable) {
        super(QCustomerOrder.class, PathMetadataFactory.forVariable(variable), "", "CUSTOMER_ORDER");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(customerId, ColumnMetadata.named("customer_id"));
        addMetadata(customerPaymentMethodId, ColumnMetadata.named("customer_payment_method_id"));
        addMetadata(totalOrderPrice, ColumnMetadata.named("total_order_price"));
        addMetadata(orderPlacedDate, ColumnMetadata.named("order_placed_date"));
        addMetadata(orderPaidDate, ColumnMetadata.named("order_paid_date"));
        addMetadata(orderStatus, ColumnMetadata.named("order_status"));
    }
}
