package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;

import java.sql.Date;

public class QCustomerOrderProduct extends RelationalPathBase<QCustomerOrderProduct> {

    public static final QCustomerOrderProduct customerOrderProduct = new QCustomerOrderProduct("CUSTOMER_ORDER_PRODUCT");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> orderId = createNumber("orderId", Long.class);
    public final NumberPath<Long> productId = createNumber("productId", Long.class);
    public final NumberPath<Integer> quantity = createNumber("quantity", Integer.class);
    public final StringPath comments = createString("comments");

    public QCustomerOrderProduct(Class<? extends QCustomerOrderProduct> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QCustomerOrderProduct(Class<? extends QCustomerOrderProduct> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QCustomerOrderProduct(String variable) {
        super(QCustomerOrderProduct.class, PathMetadataFactory.forVariable(variable), "", "CUSTOMER_ORDER_PRODUCT");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(orderId, ColumnMetadata.named("order_id"));
        addMetadata(productId, ColumnMetadata.named("product_id"));
        addMetadata(quantity, ColumnMetadata.named("quantity"));
        addMetadata(comments, ColumnMetadata.named("comments"));
    }
}
