package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.RelationalPathBase;
import org.joda.time.LocalDateTime;

public class QCustomerPaymentMethod extends RelationalPathBase<QCustomerPaymentMethod> {

    public static final QCustomerPaymentMethod customerPaymentMethod = new QCustomerPaymentMethod("CUSTOMER_PAYMENT_METHOD");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> customerId = createNumber("customerId", Long.class);
    public final DateTimePath<LocalDateTime> fromDate = createDateTime("fromDate", LocalDateTime.class);
    public final DateTimePath<LocalDateTime> toDate = createDateTime("toDate", LocalDateTime.class);

    public final StringPath paymentMethodCode = createString("paymentMethodCode");
    public final StringPath cardNumber = createString("cardNumber");
    public final StringPath otherDetails = createString("otherDetails");

    public QCustomerPaymentMethod(Class<? extends QCustomerPaymentMethod> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QCustomerPaymentMethod(Class<? extends QCustomerPaymentMethod> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QCustomerPaymentMethod(String variable) {
        super(QCustomerPaymentMethod.class, PathMetadataFactory.forVariable(variable), "", "CUSTOMER_PAYMENT_METHOD");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(customerId, ColumnMetadata.named("customer_id"));
        addMetadata(fromDate, ColumnMetadata.named("from_date"));
        addMetadata(toDate, ColumnMetadata.named("to_date"));
        addMetadata(paymentMethodCode, ColumnMetadata.named("payment_method_code"));
        addMetadata(cardNumber, ColumnMetadata.named("card_number"));
        addMetadata(otherDetails, ColumnMetadata.named("other_details"));
    }
}
