package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.PrimaryKey;
import com.querydsl.sql.RelationalPathBase;

public class QAddress extends RelationalPathBase<QAddress> {

    public static final QAddress address = new QAddress("ADDRESS");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    private final StringPath street = createString("street");
    private final StringPath zip = createString("zip");
    private final StringPath town = createString("town");
    private final StringPath state = createString("state");
    private final StringPath country = createString("country");
    private final StringPath otherDetails = createString("otherDetails");

    public QAddress(Class<? extends QAddress> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QAddress(Class<? extends QAddress> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QAddress(String variable) {
        super(QAddress.class, PathMetadataFactory.forVariable(variable), "", "ADDRESS");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(street, ColumnMetadata.named("street"));
        addMetadata(zip, ColumnMetadata.named("zip"));
        addMetadata(town, ColumnMetadata.named("town"));
        addMetadata(state, ColumnMetadata.named("state"));
        addMetadata(country, ColumnMetadata.named("country"));
        addMetadata(otherDetails, ColumnMetadata.named("other_details"));
    }
}
