package com.example.demo.qentity;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.PathMetadataFactory;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.ColumnMetadata;
import com.querydsl.sql.ForeignKey;
import com.querydsl.sql.RelationalPathBase;

import static com.example.demo.qentity.QProductL10n.productL10n;
import static com.example.demo.qentity.QSupplier.supplier;

public class QProduct extends RelationalPathBase<QProduct> {

    public static final QProduct product = new QProduct("PRODUCT");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final NumberPath<Long> supplierId = createNumber("supplierId", Long.class);
    public final NumberPath<Double> price = createNumber("price", Double.class);
    public final StringPath name = createString("name");
    public final StringPath otherProductDetails = createString("otherProductDetails");

    public ForeignKey<QSupplier> supplierFk = createForeignKey(supplierId, "id");
    public ForeignKey<QProductL10n> _productFk = createForeignKey(productL10n.productId, "product_id");


    public QProduct(Class<? extends QProduct> type, String variable, String schema, String table) {
        super(type, variable, schema, table);
    }

    public QProduct(Class<? extends QProduct> type, PathMetadata metadata, String schema, String table) {
        super(type, metadata, schema, table);
    }

    public QProduct(String variable) {
        super(QProduct.class, PathMetadataFactory.forVariable(variable), "", "PRODUCT");
        addMetadata();
    }

    protected void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id"));
        addMetadata(supplierId, ColumnMetadata.named("supplier_id"));
        addMetadata(price, ColumnMetadata.named("price"));
        addMetadata(name, ColumnMetadata.named("name"));
        addMetadata(otherProductDetails, ColumnMetadata.named("other_product_details"));
    }
}
