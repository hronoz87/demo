package com.example.demo.person;


import com.example.demo.controller.PersonController;
import com.example.demo.dao.PersonDao;
import com.example.demo.dto.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
public class PersonTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private PersonDao personDao;

    @Test
    void savePerson() throws Exception {
        Person person = new Person();
        person.setId(1L);
        person.setFirstName("John");
        person.setLastName("Johnov");
        person.setPhone("+7-999-222-4444");
        person.setEmail("ee@dd.com");
        person.setDescription("test");
        when(personDao.save(any(Person.class))).thenReturn(person);
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/persons")
                        .content(new ObjectMapper().writeValueAsString(person))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void savePerson_BadRequestBecauseEmailNotValid() throws Exception {
        Person person = new Person();
        person.setId(1L);
        person.setFirstName("John");
        person.setLastName("Johnov");
        person.setPhone("999-222-4444");
        person.setEmail("ee@dd.com");
        person.setDescription("test");
        when(personDao.save(any(Person.class))).thenReturn(person);
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/persons")
                        .content(new ObjectMapper().writeValueAsString(person))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void savePerson_BadRequestBecausePhoneNotValid() throws Exception {
        Person person = new Person();
        person.setId(1L);
        person.setFirstName("John");
        person.setLastName("Johnov");
        person.setPhone("+7-999-222-4444");
        person.setEmail("com");
        person.setDescription("test");
        when(personDao.save(any(Person.class))).thenReturn(person);
        mvc.perform(MockMvcRequestBuilders
                        .post("/api/persons")
                        .content(new ObjectMapper().writeValueAsString(person))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getAllPersonsAPI() throws Exception {
        when(personDao.findAll()).thenReturn(getPersons());
        mvc.perform(MockMvcRequestBuilders
                        .get("/api/persons/all")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*].id").exists());
    }

    @Test
    public void getPersonByIdAPI() throws Exception {
        Person person = new Person();
        person.setId(1L);
        person.setFirstName("John");
        person.setLastName("Johnov");
        person.setPhone("+7-999-222-4444");
        person.setEmail("ee@dd.com");
        person.setDescription("test");
        when(personDao.findById(anyLong())).thenReturn(person);
        mvc.perform(MockMvcRequestBuilders
                        .get("/api/persons/1", 1L)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<Person> getPersons() {
        Person one = new Person();
        one.setId(1L);
        one.setFirstName("John");
        one.setLastName("Johnov");
        one.setPhone("+7-999-222-4444");
        one.setEmail("John@dd.com");
        one.setDescription("test");
        Person two = new Person();
        two.setId(2L);
        two.setFirstName("John1");
        two.setLastName("Johnov1");
        two.setPhone("+7-999-222-4444");
        two.setEmail("John1@dd.com");
        two.setDescription("test1");
        return List.of(one, two);
    }
}
